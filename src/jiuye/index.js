mui.init({
				  pullRefresh : {
					container:"#jyzpMain",//下拉刷新容器标识，querySelector能定位的css选择器均可，比如：id、.class等
					down : {
					  style:'circle',//必选，下拉刷新样式，目前支持原生5+ ‘circle’ 样式
					  color:'#2BD009', //可选，默认“#2BD009” 下拉刷新控件颜色
					  height:'50px',//可选,默认50px.下拉刷新控件的高度,
					  range:'100px', //可选 默认100px,控件可下拉拖拽的范围
					  offset:'0px', //可选 默认0px,下拉刷新控件的起始位置
					  auto: false,//可选,默认false.首次加载自动上拉刷新一次
					  callback :pullupRefreshjyzpMain //必选，刷新函数，根据具体业务来编写，比如通过ajax从服务器获取新数据；
					}
				  }
				});
	
mui.plusReady(function () {
	//第一次自动运行一次招聘会查询
	zphSearchFn();
	//zph
	document.getElementById('zphSearch').addEventListener('tap',function () {
	        zphSearchFn();
	})
	//zpxx
	document.getElementById('zpxxSearch').addEventListener('tap',function () {
	        zpxxSearchFn();
	})
	//jyjz
	document.getElementById('jyjzSearch').addEventListener('tap',function () {
	        jyjzSearchFn();
	})
	
	
})



//下拉刷新方法		
function pullupRefreshjyzpMain() {
		mui('#jyzpMain').pullRefresh().endPulldown();
		  // mui('#main').pullRefresh().endPullupToRefresh(true); //参数为true代表没有更多数据了。
		  plus.webview.currentWebview().reload();
		}
//招聘会查询
zphSearchFn = function(){
	var zphHtml='';
	myCommonJs.productAjax(apiUrl.productURL+apiUrl.appRecruitSearch,{
		userId:localStorage.getItem("userId"),
		type:"招聘会"
	},
	function(res){ 
		if(res.data!=''){
			myCommonJs.jsonLog(res.data);
			for(var i=0;i<res.data.length;i++){
				zphHtml +=	'<a id="'
							+res.data[i].recruitId
							+'" href="javascript:;" class="aui-flex" onclick="openDetail(this)">'
							+'<div class="aui-flex-box">'
							+'<div class="aui-monthly mui-pull-right">' 
							+'<h3>进行中</h3>'
							+'</div>'
							+'<h2>'
							+res.data[i].name
							+'</h2>'
							+'<p>'
							+res.data[i].place
							+'</p>'
							+'<p>'
							+res.data[i].date
							+'</p>'
							+'</div>'
							+'</a>';
			}
			document.getElementById("zph").innerHTML=zphHtml;
		}
	},function(a,type,b){
		    
	})
}

//招聘信息查询
zpxxSearchFn = function(){
	var zpxxHtml='';
	myCommonJs.productAjax(apiUrl.productURL+apiUrl.appRecruitSearch,{
		userId:localStorage.getItem("userId"),
		type:"招聘信息"
	},
	function(res){
		if(res.data!=''){
			myCommonJs.jsonLog(res.data); 
			for(var i=0;i<res.data.length;i++){
				var fuli1Show = res.data[i].fuli1!=''?"":"none";
				var fuli2Show = res.data[i].fuli2!=''?"":"none";
				var fuli3Show = res.data[i].fuli3!=''?"":"none";
				var fuli4Show = res.data[i].fuli4!=''?"":"none";
				
				//此处拼接代码 null!=request.getParameter("title")?getRequest().getParameter("title"):"";
				zpxxHtml +='<a id="'
							+res.data[i].recruitId
							+'" href="javascript:;" class="aui-flex" onclick="openDetail(this)">'
							+'<div class="aui-flex-box">'
							+'<div class="aui-monthly mui-pull-right">' 
							+'<h3>进行中</h3>'
							+'</div>'
							+'<h2>'
							+res.data[i].name
							+'</h2>'
							+'<p>'
							+res.data[i].place
							+'</p>'
							+'<span>'
							+'<em style="display:'
							+fuli1Show
							+'">'
							+res.data[i].fuli1
							+'</em>'
							+'<em style="display:'
							+fuli2Show
							+'">'
							+res.data[i].fuli2
							+'</em>'
							+'<em style="display:'
							+fuli3Show
							+'">'
							+res.data[i].fuli3
							+'</em>'
							+'<em style="display:'
							+fuli4Show
							+'">'
							+res.data[i].fuli4
							+'</em>'
							+'</span>'
							+'<p>'
							+res.data[i].date
							+'</p>'
							+'</div>'
							+'</a>';
							
			}
			document.getElementById("zpxx").innerHTML=zpxxHtml;
		}
	},function(a,type,b){
		
	})
}

//就业讲座查询
jyjzSearchFn = function(){
	var jyjzHtml = '';
	myCommonJs.productAjax(apiUrl.productURL+apiUrl.appRecruitSearch,{
		userId:localStorage.getItem("userId"),
		type:"就业讲座"
	},
	function(res){
		if(res.data!=''){
			for(var i=0;i<res.data.length;i++){
				jyjzHtml='<a id="'
							+res.data[i].recruitId
							+'" href="javascript:;" class="aui-flex" onclick="openDetail(this)">'
							+'<div class="aui-flex-box">'
							+'<div class="aui-monthly mui-pull-right">' 
							+'<h3>进行中</h3>'
							+'</div>'
							+'<h2>'
							+res.data[i].name
							+'</h2>'
							+'<p>'
							+res.data[i].place
							+'</p>'
							+'<p>'
							+res.data[i].date
							+'</p>'
							+'</div>'
							+'</a>';
			}
		document.getElementById("jyjz").innerHTML=jyjzHtml;
		}
	},function(a,type,b){
		
	})
}

openDetail = function(e){
		mui.openWindow({
			url:"jiuyeDetail/index.html",
			id:"jiuyeDetail.html",
			extras:{recruitId:e.id}
		})	
	}


