mui.init();
			mui.plusReady(function() {
				var _thisData = plus.webview.currentWebview()
				var app = new Vue({
					el: '#mimeView',
					data: {
						userName: localStorage.getItem("userName"),
						userId: localStorage.getItem("userId"),
						userRole: localStorage.getItem("userRole"),
						userHeadPic:localStorage.getItem("headPicHttp"),
						userHeadPic1:localStorage.getItem("headPicHttp")
					},
					beforeCreate: function() {
					},
					created: function() {
						_this = this;
						if(localStorage.getItem("httpHeadimg")){
							_this.userHeadPic = localStorage.getItem("httpHeadimg");
						}else{
							_this.userHeadPic = localStorage.getItem("localHeadimg");
						}
						//请求这个页面数据
						var _this = this;
						myCommonJs.productAjax(
							apiUrl.productURL + apiUrl.appClassStudentSearch, {
								userId: localStorage.getItem("userId")
							},
							function(res) {
								
							},
							function(xhr, type, errorThrown) {
								console.log(type);
							}
						)
					},
					mounted: function() {

					},
					methods: {
						changeHeadPic:function(){//更改头像
								$(document).ready(function(){
									myCommonJs.bottomSelect();
									
								})
						},
						feedback: function() {
							mui.openWindow({
								url: '../feedback/feedback.html',
								id: 'feedback.html'
							})
						},
						schoolPage: function() {
							
							createWebview("../versionManager/index.html","versionManager.html",'关于我们');
						},
						gotoMima: function() {
							mui.openWindow({
								url: '../mimaReset/demo.html',
								id: 'mimaReset.html'
							})
						},
						gotoInfo:function(){
							createPage("../myselfInfo/index.html","myselfInfo.html",'个人信息');
						},
						xlgkAction: function() {
							createPage('http://125.46.98.226:8004/_data/index_Lookxl.aspx',"xlgk.html",'校历');
							// plus.runtime.openURL();
						}
					}
				})
				plus.webview.prefetchURLs('https://720yun.com/t/7bd2bab8muf?pano_id=693843');
				document.getElementById('outLogin').addEventListener('tap', function() {
					localStorage.removeItem("userName");
					localStorage.removeItem("userPsd");
					//退出登陆
					myCommonJs.productAjax(apiUrl.productURL1+apiUrl.logout,{
						userId:localStorage.getItem('userId'),
						userNumber:localStorage.getItem('userCode'),
						uuid:plus.device.uuid
					},function(res){
						
					},function(a,type,c){
						
					})
					
					createWebviewWrite("../login/index.html","login.html",'登陆');
				})
			})