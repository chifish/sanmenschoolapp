var nw = null;
function createPage(url,id,titleText) {
	nw = plus.webview.create(url,id,{
			popGesture: 'hide',
			titleNView: {
				backgroundColor: '#208BDD',
				progress: {
					color: '#0000FF'
				},
				titleText:titleText,
				titleColor:"#fff",
				// tags:[{tag:'font',id:'font',text:'原生子View控件',textStyles:{size:'18px'}}],
				autoBackButton:true
			// buttons:[{text:'<',float:'left',onclick:fnBack},{text:'',float:'left'}]
				
			}
		}),
		//正在加载
		nw.addEventListener('loading', function(e) {
			// console.log('Loading: ' + nw.getURL());
		}, false);
		//加载成功
		nw.addEventListener('loaded', function(e) {
			// console.log('Loaded: ' + nw.getURL());
		}, false);
		//窗口show事件
		nw.addEventListener('show', function(e) {
			// console.log('Webview Showed');
		}, false);
		//窗口隐藏事件
		nw.addEventListener('hide', function(e) {
			// console.log('Webview Hided');
		}, false);
		//关闭
		nw.addEventListener('close', function() {
			// console.log('close: ' + nw.getURL());
			nw = null;
		}, false);
		//窗口加载错误
		nw.addEventListener('error', function(e) {
			// console.log('Error: ' + nw.getURL());
		}, false);
		//窗口渲染中
		nw.addEventListener('rendering', function(e) {
			// console.log('Webview rendering');
		}, false);
		//渲染完毕
		nw.addEventListener('rendered', function(e) {
			// console.log('Webview rendered');
		}, false);
		//标题更新
		nw.addEventListener('titleUpdate', function(e) {
			// console.log('Update title: ' + e.title);
		}, false);
		//窗口触屏事件
		nw.addEventListener('touchstart', function(e) {
			console.log('touchstart'); 
			// console.log('popGesture: ' + e.type + ',' + e.result + ',' + e.progress)
		}, false);
		//窗口触屏事件
		nw.addEventListener('popGesture', function(e) {
			//		poplog.innerText='popGesture: '+e.type+','+e.result+','+e.progress;
			console.log('popGesture: ' + e.type + ',' + e.result + ',' + e.progress)
		}, false);
		//窗口进度条事件
		nw.addEventListener('progressChanged', function(e) {
			// e.progress可获取加载进度
			//		outlog.innerText='Progress Changed: '+e.progress+'%';
			// console.log('Progress Changed: ' + JSON.stringify(e));
		}, false);
		nw.appendJsFile('../../js/mui.min.js')
		nw.show('pop-in');
}

function fnBack(){
	console.log("123")
}
