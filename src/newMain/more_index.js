 mui.init({
                gestureConfig:{
                    tap: true, //默认为true
                    longtap: true//默认为false
                }
            });
//长按收藏

mui(".aui-palace").on("longtap",".aui-palace-grid",function(){
	mui.toast("已收藏");
	plus.device.vibrate(200);
	console.log(this.id);
	if(this.id!="ydoa"){
		mui.toast("已收藏");
		localStorage.setItem(this.id,this.id);
	}else{
		mui.toast("该类目不可收藏");
	}
	
})
//搜索 虚拟键盘搜索
document.getElementById('search').addEventListener('keypress',function () {
		 if(event.keyCode == "13") {
		    document.activeElement.blur();//收起虚拟键盘
			searchResult(this.value);
		    event.preventDefault(); // 阻止默认事件---阻止页面刷新
		}
		
})
//搜索 点击图标搜索
document.getElementById('searchBtn').addEventListener('tap',function () {
        var serValue = document.getElementById('search').value;
		searchResult(serValue);
})
var thisUserCode = localStorage.getItem("userCode");
console.log(thisUserCode)
// 页面图标跳转
mui(".aui-palace").on("tap", ".aui-palace-grid", function() {
			
				switch (this.id) {
					case 'gzcw':
						mui.openWindow({
							url: '../caiwu/caiwu.html',
							id: 'caiwu.html'
						})
						break;
					case 'cjcx':
						mui.openWindow({
							url: '../chengjiAction/index.html',
							id: 'chengjiAction.html'
						})
						break;
					case 'kccx':
						mui.openWindow({
							url: '../kecheng/kecheng.html',
							id: 'kecheng.html'
						})
						break;
					case 'ykt':
						mui.openWindow({
							url: '../yikatong/index.html',
							id: 'yikatong.html'
						})
						break;
					case 'swzl':
						mui.openWindow({
							url: '../lostAndFind/index.html',
							id: 'lostAndFind.html'
						})
						break;
					case 'zpq':
						mui.openWindow({
							url: '../xiaoYuanQuan/index.html',
							id: 'xiaoYuanQuan.html'
						})
						break;
					case 'tsg':
						mui.openWindow({
							url: '../tushuguan/index.html',
							id: 'tushuguan.html'
						})
						break;
					case 'hyqd':
						mui.openWindow({
							url: '../tushuguan/index.html',
							id: 'tushuguan.html'
						})
						break;
					case 'jyzp':
						mui.openWindow({
							url: '../jiuye/index.html',
							id: 'jiuye.html'
						})
						break;
					case 'xyhd':
						mui.openWindow({
							url: '../schoolActivity/index.html',
							id: 'schoolActivity.html'
						})
						break;
					case 'txl':
						mui.openWindow({
							url: '../txl/index.html',
							id: 'txl.html'
						})
						break;
					case 'wjtp':
						mui.openWindow({
							url: '../wenjuanToupiao/index.html',
							id: 'wenjuanToupiao.html'
						})
						break;	
					case 'stpj':
						mui.openWindow({
							url: '../shitangPingjia/shitangPingjia.html',
							id: 'shitangPingjia.html'
						})
						break;	
					case 'dgsx':
						launchStudy();
						break;
					case 'zxxx':
						launchStudy();
						break;
					case 'xwks':
						mui.openWindow({
							url: '../xwksBaoming/index.html',
							id: 'xwksBaoming.html'
						})
						break;	
					case 'qgzx':
						mui.openWindow({
							url: '../qinggongzhuxue/index.html',
							id: 'qinggongzhuxue.html'
						})
						break;
					case 'meetingSign':
						mui.openWindow({
							url: "../main/meetingSign/meetingSign.html",
							id: "meetingSign.html"
						})
						break;
					case 'repairBtn':
						mui.openWindow({
							url: "../sever/repairlist.html",
							id: "repairlist.html"
						})
						break;
					case 'classSelect':
						if (localStorage.getItem("userRole") == "学生") {
							mui.openWindow({
								url: "../kecheng/kecheng.html",
								id: "kecheng.html"
							})
						} else {
							mui.openWindow({
								url: "../kecheng/kecheng2.html",
								id: "kecheng2.html"
							})
						}
						break;	
					case 'chengji':
						mui.openWindow({
							url: "../chengjiAction/index.html",
							id: "chengji.html"
						})
						break;
					case 'gzcx':
						mui.openWindow({
							url: "../caiwu/caiwu.html",
							id: "caiwu.html"
						})
						break;
					case 'tsg':
						mui.openWindow({
							url: "../tushuguan/index.html",
							id: "tushuguan.html"
						})
						break;	
					case 'more':
						mui.openWindow({
							url: "../more/index.html",
							id: "more.html"
						})
						break;
					case 'xsaq':
						createWebview('../studentSecurity/index.html','studentSecurity.html','学生安全')
						break;
						// xywz
					case 'xywz':
						createWebview('https://www.smxpt.cn/','smxpt.cn','学院网站')
						break;	
					case 'jyzp':
						mui.openWindow({
							url: "../jiuye/index.html",
							id: "jiuye.html"
						})
						break;
					case 'yzb':
					createWebview('../oneSheet/index.html','oneSheet.html','一张表');
						break;
					case 'xqtj':
					createWebview('../schoolTongji/demo.html','schoolTongji.html','校情统计');
						break;	
					case 'xyyw':
					createWebview('https://www.smxpt.cn/xwzx/zhxw.htm','zhxw.htm','学院要闻');
						break;
					case 'xyw': 
						mui.openWindow({
							url: "../schoolWeb/index.html",
							id: "schoolWeb.html"
						})
						break;
					case 'ydoa': 
						plus.nativeUI.showWaiting();
						myCommonJs.getData('http://oa.smxpt.cn/authenticationmanagement/smxLogin.do?userCode='+localStorage.getItem("userCode"),{
							// userCode:00651
						},function(res){
							plus.nativeUI.closeWaiting();
							if(res.result=='success'){
								createWebview(res.oaUrl,'oApage.html','移动OA')
							}else{
								plus.nativeUI.alert("请求失败");
							}
						},function(a,type,c){
							plus.nativeUI.closeWaiting();
							console.log(type);
							plus.nativeUI.alert("请求失败");
						})
						break;
					case 'xlgk': 
						createWebview('http://www.smxpt.cn/xqzl/xxjj.htm','gkpage.html','学校概况');
						break;
					case 'yylb': 
						mui.openWindow({
							url: "../appManager/index.html",
							id: "appManager.html"
						})
						break;
					case 'fx': 
						mui.openWindow({
							url: "../foundInfo/index.html",
							id: "foundInfo.html"
						})
						break;	
					default:
						alert("建设中...") 
						break;
				}
			})
			
			
			// WEB启动学习通
    function launchStudy() {
        // window.location.href = "cxstudy://cxstudy/login?fid=xxx&fidType=xxx&token=xxx"; //唤醒并自动登录
        window.location.href = "cxstudy://"; //仅唤醒
        setTimeout(function() {
            if (!document.webkitHidden) {
				plus.runtime.openURL("https://apps.chaoxing.com/");
            }
        }, 1000);
    }
	  function searchResult(s){
		  
		if (s.search("网站") != -1||s.search("学院") != -1||s.search("学院网站") != -1) {
					  	createWebview('https://www.smxpt.cn/','smxpt.cn','学院网站');
		} else  if (s.search("会议") != -1) {
			  if (localStorage.getItem("userRole") == "学生") {
				mui.toast("您不是教师用户哦");
			  } else {
			  	mui.openWindow({
			  		url: "../main/meetingSign/meetingSign.html",
			  		id: "meetingSign.html"
			  	})
			  }
		  } else if (s.search("课程") != -1) {
			  if (localStorage.getItem("userRole") == "学生"||localStorage.getItem("userRole") == "管理员") {
			  	mui.openWindow({
			  		url: "../kecheng/kecheng.html",
			  		id: "kecheng.html"
			  	})
			  } else {
			  	mui.openWindow({
			  		url: "../kecheng/kecheng2.html",
			  		id: "kecheng2.html"
			  	})
			  }
		  	// speak("已帮您打开课程查询");
		  } else if (s.search("成绩") != -1) {
			   if (localStorage.getItem("userRole") == "学生"||localStorage.getItem("userRole") == "管理员") {
			  	mui.openWindow({
			  		url: "../chengji/chengji.html",
			  		id: "chengji.html"
			  	})
			  } else {
			  	mui.toast("您不是学生用户哦");
			  }
		  	// speak("已帮您打开成绩查询");
		  } else if (s.search("报修") != -1) {
		  	mui.openWindow({
		  		url: "../sever/repairlist.html",
		  		id: "repairlist.html"
		  	})
		  	// speak("已帮您打开学校报修");
		  } else if (s.search("图书") != -1) {
		  	mui.openWindow({
		  		url: "../tushuguan/index.html",
		  		id: "tushuguan.html"
		  	})
		  	// speak("已帮您打开图书馆");
		  } else if (s.search("一卡") != -1) {
		  	mui.openWindow({
		  		url: "../yikatong/index.html",
		  		id: "yikatong.html"
		  	})
		  	// speak("已帮您打开一卡通查询");
		  } else if (s.search("失物") != -1) {
		  	mui.openWindow({
		  		url: "../lostAndFind/index.html",
		  		id: "lostAndFind.html"
		  	})
		  	// speak("已帮您打开失物招领");
		  } else if (s.search("修改") != -1) {
		  	mui.openWindow({
		  		url: "../mimaReset/demo.html",
		  		id: "mimaReset.html"
		  	})
		  	// speak("已帮您打开修改密码");
		  }else if (s.search("工资") != -1) {
		  	mui.openWindow({
		  		url: "../caiwu/caiwu.html",
		  		id: "caiwu.html"
		  	})
		  	// speak("已帮您打开工资查询");
		  }else if (s.search("收藏") != -1) {
		  	mui.openWindow({
		  		url: "../more/index.html",
		  		id: "more.html"
		  	})
		  	// speak("已帮您打开工资查询");
		  }else if(s.search("概况") != -1) {
			  createWebview('http://www.smxpt.cn/xqzl/xxjj.htm','gkpage.html','学校概况');
		  }else if(s.search("照片") != -1||s.search("墙") != -1) {
			  mui.openWindow({
			  	url: '../xiaoYuanQuan/index.html',
			  	id: 'xiaoYuanQuan.html'
			  })
		  }else if(s.search("就业") != -1||s.search("招聘") != -1) {
			  mui.openWindow({
			  	url: '../jiuye/index.html',
			  	id: 'jiuye.html'
			  })
		  }else if(s.search("活动") != -1) {
			  mui.openWindow({
			  	url: '../schoolActivity/index.html',
			  	id: 'schoolActivity.html'
			  })
		  }else if(s.search("通讯录") != -1||s.search("通讯") != -1) {
			  if (localStorage.getItem("userRole") == "学生") {
			  	mui.toast("您不是教师用户哦");
			  } else {
				  mui.openWindow({
				  	url: '../txl/index.html',
				  	id: 'txl.html'
				  })
			  }
		  }else if(s.search("要闻") != -1) {
			  createWebview('https://www.smxpt.cn/xwzx/zhxw.htm','zhxw.htm','学院要闻');
		  }else if(s.search("发现") != -1) {
			  mui.openWindow({
			  	url: "../foundInfo/index.html",
			  	id: "foundInfo.html"
			  })
		  }else if(s.search("应用") != -1) {
			  mui.openWindow({
			  	url: "../appManager/index.html",
			  	id: "appManager.html"
			  })
		  }else if(s.search("校外报名") != -1||s.search("校外") != -1||s.search("校外考试") != -1) {
			  mui.openWindow({
			  	url: '../xwksBaoming/index.html',
			  	id: 'xwksBaoming.html'
			  })
		  }else if(s.search("勤工") != -1||s.search("助学") != -1||s.search("勤工助学") != -1) {
			  mui.openWindow({
			  	url: '../qinggongzhuxue/index.html',
			  	id: 'qinggongzhuxue.html'
			  })
		  }else if(s.search("顶岗") != -1||s.search("实习") != -1) {
			  launchStudy();
		  }else if(s.search("在线") != -1||s.search("在线学习") != -1) {
			 launchStudy();
		  }else if(s.search("问卷投票") != -1||s.search("投票") != -1||s.search("问卷") != -1) {
			  mui.openWindow({
			  	url: '../wenjuanToupiao/index.html',
			  	id: 'wenjuanToupiao.html'
			  })
		  }else if(s.search("一张表") != -1) {
			  createWebview('../oneSheet/index.html','oneSheet.html','一张表')
		  }else if(s.search("食堂评价") != -1||s.search("评价") != -1||s.search("食堂") != -1) {
			  mui.openWindow({
			  	url: '../shitangPingjia/shitangPingjia.html',
			  	id: 'shitangPingjia.html'
			  })
		  }else if(s.search("校情统计") != -1||s.search("校情") != -1||s.search("统计") != -1) {
			  createWebview('../schoolTongji/demo.html','schoolTongji.html','校情统计');
		  }else if(s.search("校园网") != -1) {
			 mui.openWindow({
			 	url: "../schoolWeb/index.html",
			 	id: "schoolWeb.html"
			 })
		  }else if(s.search("学生安全") != -1||s.search("安全") != -1) {
			  if (localStorage.getItem("userRole") == "学生") {
			  	mui.toast("您不是教师用户哦");
			  } else {
			  	createWebview('../studentSecurity/index.html','studentSecurity.html','学生安全');
			  }
		  }else if(s.search("移动OA") != -1||s.search("OA") != -1) {
			  if (localStorage.getItem("userRole") == "学生") {
			  	mui.toast("您不是教师用户哦");
			  } else {
				  plus.nativeUI.showWaiting();
				  myCommonJs.getData('http://oa.smxpt.cn/authenticationmanagement/smxLogin.do?userCode='+localStorage.getItem("userCode"),{
				  	// userCode:00651
				  },function(res){
				  	console.log("----"+JSON.stringify(res))
				  	plus.nativeUI.closeWaiting();
				  	if(res.result=='success'){
				  		createWebview(res.oaUrl,'oApage.html','移动OA')
				  	}else{
				  		plus.nativeUI.alert("请求失败");
				  	}
				  },function(a,type,c){
				  	plus.nativeUI.closeWaiting();
				  	console.log(type);
				  	plus.nativeUI.alert("请求失败");
				  })
				
			  }
		  }
	  }