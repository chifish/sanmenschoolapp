// 更新
function b() {
	t = parseInt(x.css('top'));
	y.css('top', '50px');
	x.animate({
		top: t - 50 + 'px'
	}, 'slow');
	if (Math.abs(t) == h - 50) {
		y.animate({
			top: '0px'
		}, 'slow');
		z = x;
		x = y;
		y = z;
	}
	setTimeout(b, 3000);
}
$(document).ready(function() {
	setTimeout(function() {
		$('.swap').html($('.news_li').html());
		x = $('.news_li');
		y = $('.swap');
		h = $('.news_li li').length * 50;
	}, 1000);
	setTimeout(b, 4000);


	if (localStorage.getItem("userRole") == "学生") {
		$("#gzcx").css("display", "none");
		$("#meetingSign").css("display", "none");
		$("#txl").css("display", "none");
		$("#ydoa").css("display", "none");
		$("#xsaq").css("display", "none");
	} else if (localStorage.getItem("userRole") == "教师") {
		$("#chengji").css("display", "none");
	}

})

var appNews = new Vue({
	el: "#newsList",
	data: {
		list: []
	}
})
mui.init();
mui.plusReady(function() {
	//版本更新提示
	var versionCode = 0;
	var versionURL = '';
	mui.ajax(apiUrl.productURL+apiUrl.appVersion, {
		data: {
			userId: localStorage.getItem("userId"),
		},
		dataType: 'json', //服务器返回json格式数据
		type: 'post', //HTTP请求类型
		timeout: 100000, //超时时间设置为10秒；
		headers: {
			'Content-Type': 'application/json'
		},
		success: function(data) {
			console.log("data: " + JSON.stringify(data));
			code = data.code;
			versionCode = data.versionCode;
			versionURL = data.versionUrl;
			appleUrl = data.appleUrl;
			plus.runtime.getProperty(plus.runtime.appid,function(inf){  
			    wgtVer=inf.version;  
			    console.log("当前应用版本："+wgtVer);  
				if (code>wgtVer){
					if(!mui.os.ios){ 
						plus.nativeUI.alert("已出现新版本，请立即更新下载",function(){
							// plus.runtime.openURL(versionURL);
							downWgt(versionURL)
						});
						console.log(versionURL); 
					}else{
						plus.nativeUI.alert("请前往APPStore，更新版本",function(){
							plus.runtime.openURL(appleUrl);
						});
					}
				}
			}); 

		},
		error: function(xhr, type, errorThrown) {
			//异常处理；
			console.log(type);
		}
	});
	
	// 获取新闻标题
	myCommonJs.productAjax(
			apiUrl.productURL+apiUrl.appSchoolNewsListSearchRss,
			{
				userId: localStorage.getItem("userId")
			},function(data){
				// console.log("data: " + JSON.stringify(data));
				
					// console.log(JSON.stringify(data))
					var list1 = [];
					var list2 = [];
					var list3 = [];
					var list4 = [];
					var list5 = [];
					var list = [];
					if (data.data != "" && data.data.length > 0) {
						$.each(data.data, function(i, item) {
							if ("0" == i || "1" == i) {
								list1.push(item);
							} else if ("2" == i || "3" == i) {
								list2.push(item);
							} else if ("4" == i || "5" == i) {
								list3.push(item);
							} else if ("6" == i || "7" == i) {
								list4.push(item);
							} else if ("8" == i || "9" == i) {
								list5.push(item);
							}
						});
						list.push(list1);
						list.push(list2);
						list.push(list3);
						list.push(list4);
						list.push(list5);
						appNews.list = list;
					}
				
			},
			function(a,type,c){
				console.log(typeof localStorage.getItem("userId"));
				console.log(type);
			})
	//新闻公告列表
	// mui.ajax(apiUrl.productURL+apiUrl.appSchoolNewsListSearch, {
	// 	data: {
	// 		userId: localStorage.getItem("userId"),
	// 		pageNo: 1
	// 	},
	// 	dataType: 'json', //服务器返回json格式数据
	// 	type: 'post', //HTTP请求类型
	// 	timeout: 100000, //超时时间设置为10秒；
	// 	headers: {
	// 		'Content-Type': 'application/json'
	// 	},
	// 	success: function(data) {
	// 		// console.log(JSON.stringify(data))
	// 		var list1 = [];
	// 		var list2 = [];
	// 		var list3 = [];
	// 		var list4 = [];
	// 		var list5 = [];
	// 		var list = [];
	// 		if (data.data != "" && data.data.length > 0) {
	// 			$.each(data.data, function(i, item) {
	// 				if ("0" == i || "1" == i) {
	// 					list1.push(item);
	// 				} else if ("2" == i || "3" == i) {
	// 					list2.push(item);
	// 				} else if ("4" == i || "5" == i) {
	// 					list3.push(item);
	// 				} else if ("6" == i || "7" == i) {
	// 					list4.push(item);
	// 				} else if ("8" == i || "9" == i) {
	// 					list5.push(item);
	// 				}
	// 			});
	// 			list.push(list1);
	// 			list.push(list2);
	// 			list.push(list3);
	// 			list.push(list4);
	// 			list.push(list5);
	// 			appNews.list = list;
	// 		}
	// 	},
	// 	error: function(xhr, type, errorThrown) {
	// 		//异常处理；
	// 		console.log(type); 
	// 	}
	// });


	//检测登录状态
	if (localStorage.getItem("userName") && localStorage.getItem("userId")) {
		// 欢迎回来
		mui.toast("您好，"+localStorage.getItem("userName"));
	} else {
		alert("检测到您未登陆，请登陆")
		createWebviewWrite("../login/index.html","login.html",'登陆');
	}
	mui(".t_news").on("tap", "ul", function() {
		createWebview('https://www.smxpt.cn/xwzx/zhxw.htm','zhxw.htm','学院要闻');
	});
})

// 下载wgt文件
function downWgt(wgtUrl){  
var waitObj = plus.nativeUI.showWaiting("正在下载更新,请耐心等待,请勿操作...");  
var downloaderong = plus.downloader.createDownload( wgtUrl, {filename:"_doc/update/"}, function(d,status){  
    if ( status == 200 ) {   
        console.log("下载wgt成功："+d.filename);  
        installWgt(d.filename); // 安装wgt包  
    } else {  
        console.log("下载wgt失败！");  
        plus.nativeUI.alert("下载wgt失败！");  
    }  
    plus.nativeUI.closeWaiting();  
})
// 监听下载
// downloaderong.addEventListener("statechanged",function(e){
// 	console.log(Number(e.downloadedSize/e.totalSize*100).toFixed(2)+'%');
// 		if(Number(e.downloadedSize/e.totalSize*100).toFixed(2)==25.00){
// 			plus.nativeUI.closeWaiting();  
// 			plus.nativeUI.showWaiting('下载进度:'+ Number(e.downloadedSize/e.totalSize*100).toFixed(2)+'%'); 
// 		}else if(Number(e.downloadedSize/e.totalSize*100).toFixed(2)==50.00){
// 			plus.nativeUI.closeWaiting();  
// 			plus.nativeUI.showWaiting('下载进度:'+ Number(e.downloadedSize/e.totalSize*100).toFixed(2)+'%'); 
// 		}else if(Number(e.downloadedSize/e.totalSize*100).toFixed(2)==75.00){
// 			plus.nativeUI.closeWaiting();  
// 			plus.nativeUI.showWaiting('下载进度:'+ Number(e.downloadedSize/e.totalSize*100).toFixed(2)+'%'); 
// 		}else if(Number(e.downloadedSize/e.totalSize*100).toFixed(2)==100.00){
// 			plus.nativeUI.closeWaiting();  
// 			plus.nativeUI.showWaiting('下载进度:'+ Number(e.downloadedSize/e.totalSize*100).toFixed(2)+'%'); 
// 		}
		
// },false)
downloaderong.start();  
console.log(plus.downloader.totalSize);
}

// 更新应用资源  
function installWgt(path){  
plus.nativeUI.showWaiting("安装wgt文件...");  
plus.runtime.install(path,{},function(){  
    plus.nativeUI.closeWaiting();  
    console.log("安装wgt文件成功！");  
    plus.nativeUI.alert("应用资源更新完成！",function(){  
        plus.runtime.restart();  
    });  
},function(e){  
    plus.nativeUI.closeWaiting();  
    console.log("安装wgt文件失败["+e.code+"]："+e.message);  
    plus.nativeUI.alert("安装wgt文件失败["+e.code+"]："+e.message);  
});  
}

//打开二维码
document.getElementById('ewm').addEventListener('tap', function() {
	mui.openWindow({
		url: "../main/meetingSign/barcode.html",
		id: "barcode.html"
	})
})
//打开语音
document.getElementById('yuyin').addEventListener('tap', function() {
	myCommonJs.speechBtn();
})

