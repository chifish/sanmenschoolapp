 mui.init({
                gestureConfig:{
                    tap: true, //默认为true
                    longtap: true//默认为false
                }
            });
//
//显示已收藏的应用
mui.plusReady(function(){
	if(localStorage.getItem("gzcw")){
		$("#"+localStorage.getItem("gzcw")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("cjcx")){
		$("#"+localStorage.getItem("cjcx")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("zxxx")){
		$("#"+localStorage.getItem("zxxx")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("kccx")){
		$("#"+localStorage.getItem("kccx")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("ykt")){
		$("#"+localStorage.getItem("ykt")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("swzl")){
		$("#"+localStorage.getItem("swzl")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("zpq")){
		$("#"+localStorage.getItem("zpq")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("tsg")){
		$("#"+localStorage.getItem("tsg")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("hyqd")){
		$("#"+localStorage.getItem("hyqd")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("jyzp")){
		$("#"+localStorage.getItem("jyzp")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("xyhd")){
		$("#"+localStorage.getItem("xyhd")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("txl")){
		$("#"+localStorage.getItem("txl")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("wjtp")){
		$("#"+localStorage.getItem("wjtp")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("stpj")){
		$("#"+localStorage.getItem("stpj")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("dgsx")){
		$("#"+localStorage.getItem("dgsx")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("xwks")){
		$("#"+localStorage.getItem("xwks")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("qgzx")){
		$("#"+localStorage.getItem("qgzx")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("meetingSign")){
		$("#"+localStorage.getItem("meetingSign")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("repairBtn")){
		$("#"+localStorage.getItem("repairBtn")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("classSelect")){
		$("#"+localStorage.getItem("classSelect")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("chengji")){
		$("#"+localStorage.getItem("chengji")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("gzcx")){
		$("#"+localStorage.getItem("gzcx")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("xsaq")){
		$("#"+localStorage.getItem("xsaq")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("xqtj")){
		$("#"+localStorage.getItem("xqtj")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("yzb")){
		$("#"+localStorage.getItem("yzb")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("xyyw")){
		$("#"+localStorage.getItem("xyyw")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("xyw")){
		$("#"+localStorage.getItem("xyw")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("ydoa")){
		$("#"+localStorage.getItem("ydoa")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("yylb")){
		$("#"+localStorage.getItem("yylb")).removeClass('styleDisplay');
	}
	if(localStorage.getItem("fx")){
		$("#"+localStorage.getItem("fx")).removeClass('styleDisplay');
	}
})
mui(".aui-palace").on("longtap",".aui-palace-grid",function(){
	mui.toast("已取消收藏")
	console.log(this.id);
	localStorage.removeItem(this.id);
	
})
// 页面图标跳转
mui(".aui-palace").on("tap", ".aui-palace-grid", function() {
			
				switch (this.id) {
					case 'gzcw':
						mui.openWindow({
							url: '../caiwu/caiwu.html',
							id: 'caiwu.html'
						})
						break;
					case 'cjcx':
						mui.openWindow({
							url: '../chengjiAction/index.html',
							id: 'chengjiAction.html'
						})
						break;
					case 'kccx':
						mui.openWindow({
							url: '../kecheng/kecheng.html',
							id: 'kecheng.html'
						})
						break;
					case 'ykt':
						mui.openWindow({
							url: '../yikatong/index.html',
							id: 'yikatong.html'
						})
						break;
					case 'swzl':
						mui.openWindow({
							url: '../lostAndFind/index.html',
							id: 'lostAndFind.html'
						})
						break;
					case 'zpq':
						mui.openWindow({
							url: '../xiaoYuanQuan/index.html',
							id: 'xiaoYuanQuan.html'
						})
						break;
					case 'tsg':
						mui.openWindow({
							url: '../tushuguan/index.html',
							id: 'tushuguan.html'
						})
						break;
					case 'hyqd':
						mui.openWindow({
							url: '../tushuguan/index.html',
							id: 'tushuguan.html'
						})
						break;
					case 'jyzp':
						mui.openWindow({
							url: '../jiuye/index.html',
							id: 'jiuye.html'
						})
						break;
					case 'xyhd':
						mui.openWindow({
							url: '../schoolActivity/index.html',
							id: 'schoolActivity.html'
						})
						break;
					case 'txl':
						mui.openWindow({
							url: '../txl/index.html',
							id: 'txl.html'
						})
						break;
					case 'wjtp':
						mui.openWindow({
							url: '../wenjuanToupiao/index.html',
							id: 'wenjuanToupiao.html'
						})
						break;	
					case 'stpj':
						mui.openWindow({
							url: '../shitangPingjia/shitangPingjia.html',
							id: 'shitangPingjia.html'
						})
						break;	
					case 'dgsx':
						launchStudy();
						break;
					case 'zxxx':
						launchStudy(); 
						break;	
					case 'xwks':
						mui.openWindow({
							url: '../xwksBaoming/index.html',
							id: 'xwksBaoming.html'
						})
						break;	
					case 'qgzx':
						mui.openWindow({
							url: '../qinggongzhuxue/index.html',
							id: 'qinggongzhuxue.html'
						})
						break;
					case 'meetingSign':
						mui.openWindow({
							url: "../main/meetingSign/meetingSign.html",
							id: "meetingSign.html"
						})
						break;
					case 'repairBtn':
						mui.openWindow({
							url: "../sever/repairlist.html",
							id: "repairlist.html"
						})
						break;
					case 'classSelect':
						if (localStorage.getItem("userRole") == "学生") {
							mui.openWindow({
								url: "../kecheng/kecheng.html",
								id: "kecheng.html"
							})
						} else {
							mui.openWindow({
								url: "../kecheng/kecheng2.html",
								id: "kecheng2.html"
							})
						}
						break;	
					case 'gzcx':
						mui.openWindow({
							url: "../caiwu/caiwu.html",
							id: "caiwu.html"
						})
						break;
					case 'xsaq':
						mui.openWindow({
							url: "../xiaoLi/index.html",
							id: "xiaoLi.html"
						})
						break;
					case 'jyzp':
						mui.openWindow({
							url: "../jiuye/index.html",
							id: "jiuye.html"
						})
						break;
					case 'xyhd':
						mui.openWindow({
							url: "../schoolActivity/index.html",
							id: "schoolActivity.html"
						})
						break;
					case 'yzb':
					createWebview('../oneSheet/index.html','oneSheet.html','一张表')
// 						mui.openWindow({
// 							url: "../oneSheet/index.html",
// 							id: "oneSheet.html"
// 						})
						break;
					case 'xqtj':
					createWebview('../schoolTongji/demo.html','schoolTongji.html','校情统计')
						break;	
					case 'xyyw':
						mui.openWindow({ 
							url: "../newsgonggao/news.html",
							id: "newsgonggao.html"
						})
						break;
					case 'xyw': 
						mui.openWindow({
							url: "../schoolWeb/index.html",
							id: "schoolWeb.html"
						})
						break;
						// http://h5.smxpt.cn/singleLogin.jsp?openid=oZW7_0nSP_jYoa9bYxVddS9WYm1g&loginName=null
					case 'ydoa': 
						plus.nativeUI.showWaiting();
						myCommonJs.getData('http://oa.smxpt.cn/authenticationmanagement/smxLogin.do?userCode='+localStorage.getItem("userCode"),{
							// userCode:00651
						},function(res){
							console.log("----"+JSON.stringify(res))
							plus.nativeUI.closeWaiting();
							if(res.result=='success'){
								createWebview(res.oaUrl,'oApage.html','移动OA')
							}else{
								plus.nativeUI.alert("请求失败");
							}
						},function(a,type,c){
							plus.nativeUI.closeWaiting();
							console.log(type);
							plus.nativeUI.alert("请求失败");
						})
						break;
					case 'xlgk': 
						plus.runtime.openURL('http://www.smxpt.cn/xqzl/xxjj.htm');
						break;
					case 'yylb': 
						mui.openWindow({
							url: "../appManager/index.html",
							id: "appManager.html"
						})
						break;
					case 'fx': 
						mui.openWindow({
							url: "../foundInfo/index.html",
							id: "foundInfo.html"
						})
						break;	
					default:
						alert("建设中...") 
						break;
				}
			})
			
			
			// WEB启动学习通
    function launchStudy() { 
        // window.location.href = "cxstudy://cxstudy/login?fid=xxx&fidType=xxx&token=xxx"; //唤醒并自动登录
        window.location.href = "cxstudy://"; //仅唤醒
        setTimeout(function() {
            if (!document.webkitHidden) {
				plus.runtime.openURL("https://apps.chaoxing.com/");
            }
        }, 3000);
    }