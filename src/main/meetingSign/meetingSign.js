mui.init()
mui.plusReady(function() {
		 var _thisData = plus.webview.currentWebview()
					var app = new Vue({
						el: '#mui-content',
						data: {
							meetingData:{},
							isExist:false
						},
					created:function(){
						var _this = this;
						myCommonJs.productAjax(
						apiUrl.productURL+apiUrl.meetingSearch,
						{
							userId:localStorage.getItem("userId")}, 
						function(res){
								console.log(JSON.stringify(res.data))
							if(res.data.length>0){
								 _this.meetingData = res.data;
								 _this.isExist = true;
								 if(_this.isExist){
								 	_this.isShow = 'display: block;';
									$(document).ready(function(){
											});
								 }else{
								 	$("#proJect").load("../../building/demo.html"); 
								 }
							}else{
								$("#proJect").load("../../building/demo.html");
							}
							},
						function(xhr, type, errorThrown){
							$("#proJect").load("../../building/demo.html");
						}
						)
					},mounted:function(){
					},
						methods: {
							gotoDetail:function(e){
															console.log(e.meetingId)					
														mui.openWindow({
															url:"meetingDetail.html",
															id:"meetingDetail.html",
															extras:{
																	meetingId: e.meetingId,
																	meetingTitle: e.meetingTitle,
																	meetingContent: e.meetingContent,
																	meetingState: e.meetingState,
																	meetingData: e.meetingData,
																	meetingStartTime: e.meetingStartTime,
																	meetingEndTime: e.meetingEndTime,
																	meetingSignState: e.meetingSignState,
																	meetingMessage: e.meetingMessage,
																	meetingPlace:e.meetingPlace,
																	meetingPeople: e.meetingPeople
															}
														})
													},
													gotoBarcode:function(e){
														mui.openWindow({
															url:"barcode.html",
															id:"barcode.html",
															extras:e
														})
													},
													qjAplly:function(e){
														// if(e.meetingPeople)
														myCommonJs.productAjax(apiUrl.productURL+apiUrl.appLeaveMeeting,{
															userId:localStorage.getItem("userId"),
															meetingId: e.meetingId
														},function(res){
															console.log(res)
															$(document).ready(function(){
							// 									$("#qjAplly").text("请假中");
							// 									$("#qjAplly").attr("disabled","disabled");
																plus.webview.currentWebview().reload();
															})
														},function(a,type,c){
															console.log(type)
														})
													}
						}
					})
})
