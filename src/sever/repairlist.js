mui.init();
mui.plusReady(function() {
var idShow = false;
plus.nativeUI.showWaiting();
	mui.ajax(apiUrl.productURL+apiUrl.appRepairSearch, {
				data:{
					userId:localStorage.getItem("userId")
				},
			dataType: 'json', //服务器返回json格式数据
			type: 'post', //HTTP请求类型
			timeout: 100000, //超时时间设置为10秒；
			headers: {
				'Content-Type': 'application/json'
			},
			success: function(data) {
				plus.nativeUI.closeWaiting();
				console.log(JSON.stringify(data.data))
				// data.code == 10001  true
				if (data.code == 10001) {
					console.log(JSON.stringify(data.data.maintenanceTime))
					idShow = true;
					var app = new Vue({
						el: '#repairContent',
						data: {
							repairDatas: data.data,
							idShow: idShow
						},methods:{
							newRepair:function(){
								mui.openWindow({
									url:"repair/repair.html",
									id:"repair.html"
								});
								},
								optionRepair:function(e){
									mui.openWindow({
										url:"repair/opotionPage.html",
										id:"opotionPage.html",
										extras:{
											userId: localStorage.getItem("userId"),
											repairId:e.repairId,
											repairCode:e.repairCode,
											repairState:e.repairState,
											faultType:e.faultType,
											faultLocatin:e.faultLocatin,
											repairTime:e.repairTime,
											faultMessages: e.faultMessages,
											rpairUserName:e.rpairUserName,
											maintenanceTime:e.maintenanceTime,
											faultImages:e.faultImages
											}
									});
									}
						}
					})
				}
	
			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
				console.log(type);
			}
		});
})
