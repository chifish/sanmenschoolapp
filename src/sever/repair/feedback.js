/*!
 * ======================================================
 * FeedBack Template For MUI (http://dev.dcloud.net.cn/mui)
 * =======================================================
 * @version:1.0.0
 * @author:cuihongbao@dcloud.io
 */
(function() {
	var index = 1;
	var size = null;
	var imageIndexIdNum = 0;
	var starIndex = 0;
	var flag= 0;
	var feedback = {
		question: document.getElementById('question'), 
		contact: document.getElementById('contact'), 
		imageList: document.getElementById('image-list'),
		submitBtn: document.getElementById('submit')
	};
	var url =apiUrl.productURL+apiUrl.appRepairApply;
	feedback.files = [];
	feedback.uploader = null;  
	feedback.deviceInfo = null; 
	mui.plusReady(function() {
		//设备信息，无需修改
		feedback.deviceInfo = {
			appid: plus.runtime.appid, 
			imei: plus.device.imei, //设备标识
			images: feedback.files, //图片文件
			p: mui.os.android ? 'a' : 'i', //平台类型，i表示iOS平台，a表示Android平台。
			md: plus.device.model, //设备型号
			app_version: plus.runtime.version,
			plus_version: plus.runtime.innerVersion, //基座版本号
			os:  mui.os.version,
			net: ''+plus.networkinfo.getCurrentType()
		}
	});
	/**
	 *提交成功之后，恢复表单项 
	 */
	feedback.clearForm = function() {
		
		feedback.question.value = '';
		feedback.contact.value = '';
		feedback.imageList.innerHTML = '';
		feedback.newPlaceholder();
		feedback.files = [];
		index = 0;
		size = 0;
		imageIndexIdNum = 0;
		starIndex = 0;
		//清除所有星标
		mui('.icons i').each(function (index,element) {
			if (element.classList.contains('mui-icon-star-filled')) {
				element.classList.add('mui-icon-star')
	  			element.classList.remove('mui-icon-star-filled')
			}
		})
		
		
	};
	
	
	feedback.getFileInputArray = function() {
		return [].slice.call(feedback.imageList.querySelectorAll('.file'));
	};
	
	
	feedback.addFile = function(path) {
		feedback.files.push({name:"images"+index,path:path,id:"img-"+index});
		index++;
	};
	
	
	/**
	 * 初始化图片域占位
	 */
	feedback.newPlaceholder = function() {
		var fileInputArray = feedback.getFileInputArray();
		if (fileInputArray &&
			fileInputArray.length > 0 &&
			fileInputArray[fileInputArray.length - 1].parentNode.classList.contains('space')) {
			return;
		};
		imageIndexIdNum++;
		var placeholder = document.createElement('div');
		placeholder.setAttribute('class', 'image-item space');
		var up = document.createElement("div");
		up.setAttribute('class','image-up')
		//删除图片
		var closeButton = document.createElement('div');
		closeButton.setAttribute('class', 'image-close');
		closeButton.innerHTML = 'X';
		closeButton.id = "img-"+index;
		//小X的点击事件
		closeButton.addEventListener('tap', function(event) {
			setTimeout(function() {
				for(var temp=0;temp<feedback.files.length;temp++){
					if(feedback.files[temp].id==closeButton.id){
						feedback.files.splice(temp,1);
					}
				}
				feedback.imageList.removeChild(placeholder);
			}, 0);
			return false;
		}, false);
		
		//
		var fileInput = document.createElement('div');
		fileInput.setAttribute('class', 'file');
		fileInput.setAttribute('id', 'image-' + imageIndexIdNum);
		fileInput.addEventListener('tap', function(event) {
			var self = this;
			var index = (this.id).substr(-1);
			
			plus.gallery.pick(function(e) {
				var name = e.substr(e.lastIndexOf('/') + 1);
					
				plus.zip.compressImage({
					src: e,
					dst: '_doc/' + name,
					overwrite: true,
					quality: 50
				}, function(zip) {
					size += zip.size  
					if (size > (10*1024*1024)) {
						return mui.toast('文件超大,请重新选择~');
					}
					if (!self.parentNode.classList.contains('space')) { //已有图片
						feedback.files.splice(index-1,1,{name:"images"+index,path:e});
					} else { //加号
						placeholder.classList.remove('space');
						feedback.addFile(zip.target);
						feedback.newPlaceholder();
					}
					up.classList.remove('image-up');
					placeholder.style.backgroundImage = 'url(' + zip.target + ')';
				}, function(zipe) {
					mui.toast('压缩失败！')
				});
				
				
			}, function(e) {
				mui.toast(e.message);
			},{});
			// console.log('placeholder:'+placeholder)
		}, false);
// 		// var file = fileInput.files[0] 
// 		console.log(fileInput.files)
		placeholder.appendChild(closeButton);
		placeholder.appendChild(up);
		placeholder.appendChild(fileInput);
		// console.log(fileInput)
		feedback.imageList.appendChild(placeholder);
		
// 		var urlImage =  $("#image-2").attr("src"); 
// 		console.log(urlImage)
		
	};
	
	
	feedback.newPlaceholder();
	feedback.submitBtn.addEventListener('tap', function(event) {
		var regu = "/s/"
			var re = new RegExp(regu)
			if (!re.test(feedback.contact.value)){ 
				plus.nativeUI.toast('填写内容不可为空')
				return; 
			}
			if (!re.test($("#phone").val())){
				plus.nativeUI.toast('填写内容不可为空')
				return; 
			}
			if (!re.test(feedback.question.value)){
				plus.nativeUI.toast('填写内容不可为空')
				return; 
			}
		if(!feedback.contact.value){
			return mui.toast('故障地点不可为空');
		}
		if(!$("#phone").val()){
			return mui.toast('联系方式不可为空');
		}
		if (!feedback.question.value) {
			return mui.toast('故障说明不可为空');
		}
		if (feedback.question.value.length > 200 || feedback.contact.value.length > 200) {
			return mui.toast('信息超长,请重新填写~');
		}
		//判断网络连接
		if(plus.networkinfo.getCurrentType()==plus.networkinfo.CONNECTION_NONE){
			return mui.toast("连接网络失败，请稍后再试");
		}
		feedback.send(mui.extend({}, feedback.deviceInfo, {
			phone: $("#phone").val(),
			faultMessages:feedback.question.value,
			faultType:$("#repairType").find('option:selected').val(),//故障类型
			faultLocation: feedback.contact.value,//故障地点
			images: feedback.files,
			userId:localStorage.getItem("userId"),
			score:starIndex
		})) 
	}, false);
	feedback.send = function(content) {
		feedback.uploader = plus.uploader.createUpload(url, {
			method: 'POST'
		}, function(upload, status) {
//			plus.nativeUI.closeWaiting()
			console.log("upload cb:"+upload.responseText);
			if(status==200){
				var data = JSON.parse(upload.responseText);
				//上传成功，重置表单
				if (data.ret === 0 && data.desc === 'Success') {
//					mui.toast('反馈成功~')
					console.log("upload success");
//					feedback.clearForm();
				}
			}else{
			}
			
		});
		//添加上传数据
		mui.each(content, function(index, element) {
			if (index !== 'images') {
				feedback.uploader.addData(index, element)
			} 
		});
		
		//添加上传文件
		mui.each(feedback.files, function(index, element) {
			var f = feedback.files[index];
			feedback.uploader.addFile(f.path, {
				key: f.name
			});
		});
		//开始上传任务
		feedback.uploader.start();
//		plus.nativeUI.showWaiting();
		mui.alert(" 报修成功，点击确定关闭","报修","确定",function () {
					feedback.clearForm();
					mui.back();
				});
	};
	
	 //应用评分
	 mui('.icons').on('tap','i',function(){
	  	var index = parseInt(this.getAttribute("data-index"));
	  	var parent = this.parentNode;
	  	var children = parent.children;
	  	if(this.classList.contains("mui-icon-star")){
	  		for(var i=0;i<index;i++){
  				children[i].classList.remove('mui-icon-star');
  				children[i].classList.add('mui-icon-star-filled');
	  		}
	  	}else{
	  		for (var i = index; i < 5; i++) {
	  			children[i].classList.add('mui-icon-star')
	  			children[i].classList.remove('mui-icon-star-filled')
	  		}
	  	}
	  	starIndex = index;
  });
  
  	//选择快捷输入
	mui('.mui-popover').on('tap','li',function(e){
	  document.getElementById("question").value = document.getElementById("question").value + this.children[0].innerHTML;
	  mui('.mui-popover').popover('toggle')
	}) 
})();


function appendFile(path,type,callback) {
path = path.replace(/_/, "../");
var img = new Image();
img.src = path;
img.onload = function() {
	alert("111")
var that = this;
//生成比例
var w = that.width,
h = that.height,
scale = w / h;
w = 480 || w; //480 你想压缩到多大
h = w / scale;
//生成canvas
var canvas = document.createElement('canvas');
var ctx = canvas.getContext('2d');
$(canvas).attr({
width: w,
height: h
});
ctx.drawImage(that, 0, 0, w, h);
var base64 = canvas.toDataURL('image/'+type, 1 || 1); //1z 表示图片质量，越低越模糊。
// alert(base64);
files.push({
name: "file" + index,
pic: base64
}); // 把base64数据丢进数组，上传要用。
index++;
var pic = document.getElementById("x");
pic.src = base64; //这里丢到img 的 src 里面就能看到效果了
		
		callback?callback(base64):null;
}
		
}

//传入图片路径，返回base64
		function getBase64(img){
		    function getBase64Image(img,width,height) {//width、height调用时传入具体像素值，控制大小 ,不传则默认图像大小
		      var canvas = document.createElement("canvas");
		      canvas.width = width ? width : img.width;
		      canvas.height = height ? height : img.height;
		      var ctx = canvas.getContext("2d");
		      ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
		      var dataURL = canvas.toDataURL();
		      return dataURL;
		    }
		    var image = new Image();
		    image.crossOrigin = '';
		    image.src = img;
		    var deferred=$.Deferred();
		    if(img){
		      image.onload =function (){
		        deferred.resolve(getBase64Image(image));//将base64传给done上传处理
		      }
		      return deferred.promise();//问题要让onload完成后再return sessionStorage['imgTest']
		    }
		  }