mui.init();
mui.plusReady(function() {
	var _thisPage = plus.webview.currentWebview();
	// console.log(JSON.stringify(_thisPage.faultImages[0].faultImage));
	var app = new Vue({
		el: '#optionRepair',
		data: {
			userId: _thisPage.userId,
			repairId: _thisPage.repairId,
			repairCode: _thisPage.repairCode,
			repairState: _thisPage.repairState,
			faultType: _thisPage.faultType,
			faultLocatin: _thisPage.faultLocatin,
			faultMessages: _thisPage.faultMessages,
			rpairUserName: _thisPage.rpairUserName,
			maintenanceTime: _thisPage.maintenanceTime,
			faultImages: _thisPage.faultImages,
			imageRootUrl: apiUrl.rootProductUrl
		},
		methods: {
			cancelRepair: function(e) {
				mui.ajax(apiUrl.productURL + apiUrl.appDisplay, {
					data: {
						userId: localStorage.getItem("userId"),
						repairId: e
					},
					dataType: 'json', //服务器返回json格式数据
					type: 'post', //HTTP请求类型
					timeout: 10000, //超时时间设置为10秒；
					headers: {
						'Content-Type': 'application/json'
					},
					success: function(data) {
						mui.toast(data.msg);
						// data.code == 10001  true
						if (data.code == 10001) {
							console.log(JSON.stringify(data.data.repairState))
							idShow = true;
							mui.back();
							plus.webview.getWebviewById('repairlist.html').reload();
						}
					},
					error: function(xhr, type, errorThrown) {
						//异常处理；
						console.log(type);
					}
				});
			},
			backAction: function() {
				mui.back();
				plus.webview.getWebviewById('repairlist.html').reload()
			},
			preview: function(e) {
				console.log(JSON.stringify(e))
				let imageUrlarr = new Array();
				for (let i = 0; i < e.length; i++) {
					console.log(e[i].faultImage);
					imageUrlarr.push(apiUrl.rootProductUrl + e[i].faultImage)
				}
				console.log(imageUrlarr)
				plus.nativeUI.previewImage(imageUrlarr, {
					background: '#000',
					current: 0,
					indicator: 'number',
					loop: "true"
				});
			}
		}
	})

	$(document).ready(function() {
		//应用评分
		var starIndex = 0; //starIndex这个值根据数据库的拿到的值 
		console.log(app.repairId)
		myCommonJs.productAjax(apiUrl.productURL + apiUrl.appDisRepairSearch, {
			userId: localStorage.getItem("userId"),
			repairId: app.repairId
		}, function(res) {
			starIndex = parseInt(res.data[0].star);
			if (starIndex > 0) {
				for (var i = 0; i <= starIndex; i++) {
					$('#i' + i).addClass('mui-icon-star-filled');
				}
			} else {
				mui('.icons').on('tap', 'i', function() {
					var index = parseInt(this.getAttribute("data-index"));
					var parent = this.parentNode;
					var children = parent.children;
					if (this.classList.contains("mui-icon-star")) {
						for (var i = 0; i < index; i++) {
							children[i].classList.remove('mui-icon-star');
							children[i].classList.add('mui-icon-star-filled');
						}
					} else {
						for (var i = index; i < 5; i++) {
							children[i].classList.add('mui-icon-star')
							children[i].classList.remove('mui-icon-star-filled')
						}
					}
					starIndex = index;
					console.log("报修id:" + app.repairId)
					plus.nativeUI.alert("评分已完成", function() {
						myCommonJs.productAjax(apiUrl.productURL + apiUrl.appJudgeApply, {
								userId: localStorage.getItem("userId"),
								repairId: app.repairId,
								star: starIndex
							},
							function(res) {
								console.log("打分成功:" + JSON.stringify(res))
							},
							function() {})
					})
					console.log(starIndex);
				});
			}
		}, function() {})



	})
})
