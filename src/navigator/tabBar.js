/*
 	作者：程驰
 *	名称：导航页面生成子页面
 * 	参数：{
						url:"../mainView/Home/HomeView.html",
						id:"HomeView.html",
						styles:{top:'44px' , bottom:'44px' ,width:'100%',left:"0%"}
					}
 * 	备注：
 * */
(function(tabBar) {
		tabBar.subpages = function(pageJson) {
			mui.init({
				subpages: [
					{
						url: "../newMain/index.html",
						id: "main.html",
						styles: {
							top: '0px',
							bottom: '48px',
							width: '100%',
							left: "0"
						}
					},
					{
						url: "../newCard/index.html",
						id: "gonggao.html",
						styles: {
							top: '0',
							bottom: '48px',
							width: '100%',
							left: "100%"
						}
					},
					{
						url: "../newMine/index.html",
						id: "mine.html",
						styles: {
							top: '0',
							bottom: '48px',
							width: '100%',
							left: "200%"
						}
					}
				]
			})
		}


		tabBar.changpages = function() {
			document.getElementById('sub1').addEventListener('tap', function() {
				changSub(1);
			})
			document.getElementById('sub3').addEventListener('tap', function() {
				changSub(3);
			})
			document.getElementById('sub4').addEventListener('tap', function() {
				changSub(4);
			})
		}


		/*
		 	作者：程驰
		 *	名称：导航页面左右拖动
		 * 	参数：当前页面的前后页面的ID以及按钮id
		 * 	备注：需要导入h.js或jq配合使用
		 * */
		tabBar.dragPages = function(prevPageId, nextPageId, prevItemId, nextItemId) {
			var _index, _self, _next, _prev;
			_self = plus.webview.currentWebview();
			_next = plus.webview.getWebviewById(nextPageId);
			_index = plus.webview.currentWebview().parent();
			_prev = plus.webview.getWebviewById(prevPageId);
			_self.drag({
				direction: "left",
				moveMode: "followFinger"
			}, {
				view: _next,
				moveMode: "follow"
			}, function(e) {
				if (e.type == 'end' && e.result) {
					//    			 	console.log(JSON.stringify(_index) )
					_index.evalJS('h("#navtab").find("a").removeClass("mui-active"); h("#' + nextItemId +
						'").addClass("mui-active");');
				}
			})
			_self.drag({
				direction: "right",
				moveMode: "followFinger"
			}, {
				view: _prev,
				moveMode: "follow"
			}, function(e) {
				if (e.type == 'end' && e.result) {
					_index.evalJS('h("#navtab").find("a").removeClass("mui-active");h("#' + prevItemId +
						'").addClass("mui-active");');
				}
			})
		}

		tabBar.subDragpages = function(prevPageId) {
			var _index, _self, _next, _prev;
			_self = plus.webview.currentWebview();
			_index = plus.webview.getLaunchWebview();
			_prev = plus.webview.getWebviewById(prevPageId);
			_self.drag({
				direction: "left",
				moveMode: "followFinger"
			}, {
				view: _prev,
				moveMode: "follow"
			})
		}


		


		var server = ""; 
			}(window.tabBar = {}));






			function changSub(index) {
				var sub1 = plus.webview.getWebviewById('main.html');
				var sub3 = plus.webview.getWebviewById('gonggao.html');
				var sub4 = plus.webview.getWebviewById('mine.html');
				sub1.setStyle({
					left: (1 - index) * 100 + '%'
				});
				sub3.setStyle({
					left: (3 - index) * 100 + "%"
				});
				sub4.setStyle({
					left: (4 - index) * 100 + "%"
				});
			}

			//创建子页面
			tabBar.subpages();
			//添加跳转方法
			tabBar.changpages();




			
