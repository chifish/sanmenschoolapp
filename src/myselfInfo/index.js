//个人信息
mui.init();
mui.plusReady(function () {
    var app = new Vue({
    	el:"#info",
		data:{
			userName: localStorage.getItem("userName"),
			userHeadPic:localStorage.getItem("headPicHttp"),
			userGender:'',
			userNation:'',
			userDepartment:'',
			userMajor:'',
			userGrade:'',
			userClass:'',
			userBirthDay:'',
			userPhone:'',
			userIntegral:''
			
		},methods:{
			setPhone:function(){
				var btnArray = ['取消', '确定'];
				mui.prompt('请输入您手机号：', '', '手机号填写', btnArray, function(e) {
					
					console.log(myCommonJs.isPhoneNum(e.value))
					
					if(e.index==1&&myCommonJs.isPhoneNum(e.value)){
						myCommonJs.productAjax(apiUrl.productURL+apiUrl.appPhoneApply,{
							userId:localStorage.getItem("userId"),
							phone:e.value
						},function(res){
							console.log("566:"+JSON.stringify(res))
							plus.webview.currentWebview().reload();
						})
					}
				})
				
			}
		}
    })
	
	//查询个人信息
	myCommonJs.productAjax(apiUrl.productURL + apiUrl.appInfoSearch,{
		userId:localStorage.getItem("userId")
	},function(res){
		console.log(JSON.stringify(res)); 
		if(res.data.length>0){
			app.userGender = res.data[0].gender;
			app.userNation=res.data[0].nation;
			app.userDepartment = res.data[0].yx;
			app.userMajor = res.data[0].zy;
			app.userGrade = res.data[0].nj;
			app.userClass = res.data[0].bj;
			app.userBirthDay = res.data[0].birth;
			app.userPhone = res.data[0].phone;
			app.userIntegral = res.data[0].integral;
		}
		
	},function(a,type,c){
		console.log(type);
	})
	
	
})