mui.init({
				  pullRefresh : {
					container:"#jyzpMain",//下拉刷新容器标识，querySelector能定位的css选择器均可，比如：id、.class等
					down : {
					  style:'circle',//必选，下拉刷新样式，目前支持原生5+ ‘circle’ 样式
					  color:'#2BD009', //可选，默认“#2BD009” 下拉刷新控件颜色
					  height:'50px',//可选,默认50px.下拉刷新控件的高度,
					  range:'100px', //可选 默认100px,控件可下拉拖拽的范围
					  offset:'0px', //可选 默认0px,下拉刷新控件的起始位置
					  auto: false,//可选,默认false.首次加载自动上拉刷新一次
					  callback :pullupRefreshjyzpMain //必选，刷新函数，根据具体业务来编写，比如通过ajax从服务器获取新数据；
					},up: {
						height: 50, //可选.默认50.触发上拉加载拖动距离
						auto: false, //可选,默认false.自动上拉加载一次
						contentrefresh: "正在加载...", //可选，正在加载状态时，上拉加载控件上显示的标题内容
						contentnomore: '没有更多数据了', //可选，请求完毕若没有更多数据时显示的提醒内容；
						callback: upRefresh //必选，刷新函数，根据具体业务来编写，比如通过ajax从服务器获取新数据；
					}
				  }
				});
	
mui.plusReady(function () {
	//第一次自动运行一次校外报名查询
	zphSearchFn();
	//bmc
	document.getElementById('zphSearch').addEventListener('tap',function () {
	        zphSearchFn();
	})
})


var pageNum=1;
//下拉刷新方法		
function pullupRefreshjyzpMain() {
		mui('#jyzpMain').pullRefresh().endPulldown();
		  // mui('#main').pullRefresh().endPullupToRefresh(true); //参数为true代表没有更多数据了。
		  plus.webview.currentWebview().reload();
		  zphSearchFn();
		}
		//上拉加载方法
function upRefresh() {
	// mui('#jyzpMain').pullRefresh().endPullupToRefresh(true); //关闭上拉加载
	//to do
	pageNum++;
	addNews(pageNum);
}		
//招聘会查询

zphSearchFn = function(){
	var zphHtml='';
	myCommonJs.productAjax(apiUrl.productURL+apiUrl.appOutSchoolExamSearch,{
		userId:localStorage.getItem("userId"),
		pageNo:1
		// type:"招聘会"
	},
	function(res){ 
		myCommonJs.jsonLog(res.data);
		if(res.data!=''){
			myCommonJs.jsonLog(res.data);
			var state='';
			for(var i=0;i<res.data.length;i++){
					if(res.data[i].hasApply){
						state=res.data[i].applyState
						display = "none;"
					}else{
						state="可报名";
						display = " ";
					}
				zphHtml +=	'<a id="'
							+res.data[i].schoolNewId
							+'" href="javascript:;" class="aui-flex">'
							+'<div class="aui-flex-box">'
							+'<div class="aui-monthly mui-pull-right">' 
							+'<h3>'
							+state
							+'</h3>'
							+'</div>'
							+'<h2>'
							+res.data[i].project
							+'</h2>'
							+'<p id="'
							+res.data[i].schoolNewId
							+'" onclick="detail(this)">'
							+res.data[i].contents.slice(1,10)+'...'
							+'</p>'
							+'<button id="'
							+res.data[i].schoolNewId
							+'" type="button" class="mui-btn mui-btn-blue mui-pull-right" style="display:'
							+display
							+' " onclick="baoming(this)">报名</button>'
							+'<p>'
							+res.data[i].date
							+'</p>'
							+'</div>'
							+'</a>';
			}
			document.getElementById("zph").innerHTML=zphHtml;
		}
	},function(a,type,b){
		    console.log(type)
	})
}


//上拉分页查询
var addNews = function (index){
	mui.ajax(apiUrl.debugURL+productURL.appOutSchoolExamSearch,{
		data:{
			userId:localStorage.getItem("userId"),
			pageNo: index
		},
		dataType: 'json', //服务器返回json格式数据
		type: 'post', //HTTP请求类型
		timeout: 100000, //超时时间设置为10秒；
		headers: {
			'Content-Type': 'application/json'
		},
		success: function(data) {
			// console.log(JSON.stringify(data.data))
			if (data.data != "" && data.data.length > 0) {
				// if(pageNum != "1"){
				$.each(data.data,function(i,item){
					if(item.hasApply){
						state=item.applyState;
						display= "none"; 
					}else{
						state="可报名";
						display= ""; 
					}
					var _html = '<a id="'
							+item.schoolNewId
							+'" href="javascript:;" class="aui-flex">'
							+'<div class="aui-flex-box">'
							+'<div class="aui-monthly mui-pull-right">' 
							+'<h3>'
							+state
							+'</h3>'
							+'</div>'
							+'<h2>'
							+item.project
							+'</h2>'
							+'<p id="'
							+item.schoolNewId
							+'" onclick="detail(this)">'
							+item.contents.slice(1,10)+'...'
							+'</p>'
							+'<button id="'
							+item.schoolNewId
							+'" type="button" class="mui-btn mui-btn-blue mui-pull-right" style="display:'
							+display
							+' " onclick="baoming(this)">报名</button>'
							+'<p>'
							+item.date
							+'</p>'
							+'</div>'
							+'</a>';
					$('#zph').append(_html);
				})
				// }
				// pageNum += 1;
				mui('#jyzpMain').pullRefresh().endPullupToRefresh(false);
			}else{
				mui('#jyzpMain').pullRefresh().endPullupToRefresh(true);
			}
			
		},
		error: function(xhr, type, errorThrown) {
			//异常处理；
			console.log(type);
		}
	});
}
		
		
		
baoming = function(e){
		mui.openWindow({
			url:'xwbmForm/index.html',
			id:"xwbmForm.html",
			extras:{bmId:e.id}
		})
	}

detail =  function(e){
		mui.openWindow({
			url:'xwbmDetail/index.html',
			id:"xwbmDetail.html",
			extras:{schoolNewId:e.id}
		})
	}
