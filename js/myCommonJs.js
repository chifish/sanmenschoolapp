//创建一个常见的工具类js
var myCommonJs = {};

myCommonJs.productAjax = function(url, params, successFunction, errorFunction) { //params:Json类参数
	mui.ajax(url, {
		data: params,
		dataType: 'json', //服务器返回json格式数据
		type: 'post', //HTTP请求类型
		timeout: 10000, //超时时间设置为10秒；
		headers: {
			'Content-Type': 'application/json'
		},
		success: successFunction,
		error: errorFunction
	});
}
myCommonJs.getData = function(url, params, successFunction, errorFunction) { //params:Json类参数
	mui.ajax(url, {
		data: params,
		dataType: 'json', //服务器返回json格式数据
		type: 'get', //HTTP请求类型
		timeout: 10000, //超时时间设置为10秒；
		headers: {
			'Content-Type': 'application/json'
		},
		success: successFunction,
		error: errorFunction
	});
}
myCommonJs.debugAjax = function(url, params, timeout, successFunction, errorFunction) { //params:Json类参数
	mui.ajax(url, {
		data: params,
		dataType: 'json', //服务器返回json格式数据
		type: 'post', //HTTP请求类型
		timeout: timeout, //超时时间设置为10秒；
		headers: {
			'Content-Type': 'application/json'
		},
		success: successFunction,
		error: errorFunction
	});
}

//cc 判断为空$.isEmptyObject(a)
myCommonJs.isEmptyFunction = function(e) {
	if (e === "" || e == null || e === undefined || $.isEmptyObject(e) || e.length == 0 || $.trim(e)) {
		console.log("22:" + e)
		return true;
	} else {
		return false;
	}
}
// cc 判断非空
myCommonJs.isNotEmptyFunction = function(e) {
	if (e !== "" && e !== null && e !== undefined && !$.isEmptyObject(e) && e.length != 0 && !$.trim(e) && e != NaN) {
		console.log("12")
		return true;
	}
}

//获取照片
myCommonJs.bottomSelect = function() {
	if (mui.os.plus) {
		var a = [{
			title: "拍照" //这些都是可以点击的选项的内容
		}, {
			title: "从手机相册选择"
		}];
		plus.nativeUI.actionSheet({
			title: "修改用户头像", //这里就是这个弹窗的title
			cancel: "取消", //取消按钮
			buttons: a
		}, function(b) { /*actionSheet 按钮点击事件*/
			//这里面就是触发的事件，比如想做一个选择性别的弹窗，就可以通过a[b.index].title来获取当前点击了男还是女，注意，此处的b.index是从1开始的。要从0开始的话，要记得减1.
			switch (b.index) {
				case 1:
					myCommonJs.appendByCamera();
					break;
				case 2:
					myCommonJs.appendByGallery();
					break;
			}
		})

	}
}

//拍照添加文件
myCommonJs.appendByCamera = function() {
	plus.camera.getCamera().captureImage(function(e) {
		plus.io.resolveLocalFileSystemURL(e, function(entry) {
			var path = entry.toLocalURL();
			console.log(path);
			document.getElementById('headimg').src = path;
			localStorage.setItem("localHeadimg", path);
			myCommonJs.uploadFile(apiUrl.productURL + apiUrl.changeHeadImage);
		}, function(e) {
			console.log("读取拍照文件错误：" + e.message);
		});
	});
}

// 从相册添加文件
myCommonJs.appendByGallery = function() {
	plus.gallery.pick(function(path) {
		document.getElementById('headimg').src = path;
		localStorage.setItem("localHeadimg", path);
		myCommonJs.uploadFile(apiUrl.productURL + apiUrl.changeHeadImage);
		console.log("读取文件path：" + path);
	});
}

//上传文件
myCommonJs.uploadFile = function(server) {
	//服务端接口路径
	var server = server;
	console.log(server)
	//获取图片元素
	var files = document.getElementById('headimg');
	// 上传文件
	function upload() {
		console.log(files.src);
		var wt = plus.nativeUI.showWaiting();
		var task = plus.uploader.createUpload(server, {
				method: "POST"
			},
			function(t, status) { //上传完成
				if (status == 200) {
					alert("上传成功");
					wt.close(); //关闭等待提示按钮http://125.46.98.227:12333/smxzyapp/core/
					localStorage.setItem("httpHeadimg", "http://125.46.98.227:12333/smxzyapp/opt/data/avatarDir/" + localStorage.getItem(
						"userName") + "jpg");
				} else {
					alert("上传失败");
					wt.close(); //关闭等待提示按钮
				}
			}
		);
		//添加其他参数
		task.addData("userId", localStorage.getItem("userId"));
		task.addFile(files.src, {
			key: 'name'
		});
		task.start();
	}
	upload();
}

//检查图片是否存在
myCommonJs.isExistImg = function(imgurl, success, err) {
	var ImgObj = new Image(); //判断图片是否存在  
	ImgObj.onload = function() {
		console.log(ImgObj.width, ImgObj.height);
		success && success(ImgObj)
	}
	ImgObj.onerror = function() {
		console.log('error');
		err && err(ImgObj)
	}
	ImgObj.src = imgurl;
}

//语音识别			
myCommonJs.speechBtn = function() {
	var text = null;
	// mui.toast( "进入语音识别：" );
	if (plus.os.name == 'Android' && navigator.userAgent.search('StreamApp') > 0) {
		plus.nativeUI.toast('当前环境暂不支持语音识别插件');
		return;
	}
	var options = {};
	options.engine = 'iFly';
	mui.toast("开始语音识别：");
	plus.speech.startRecognize(options, function(s) {
		mui.toast(s);
		
		if (s.search("网站") != -1||s.search("学院") != -1||s.search("学院网站") != -1) {
			createWebview('https://www.smxpt.cn/','smxpt.cn','学院网站');
		}else if (s.search("会议") != -1) {
					  if (localStorage.getItem("userRole") == "学生") {
						mui.toast("您不是教师用户哦");
					  } else {
						  mui.openWindow({
						  	url: "../main/meetingSign/meetingSign.html",
						  	id: "meetingSign.html"
						  })
					  	mui.toast("您不是教师用户哦");
					  }
		} else if (s.search("课程") != -1) {
					  if (localStorage.getItem("userRole") == "学生") {
					  	mui.openWindow({
					  		url: "../kecheng/kecheng.html",
					  		id: "kecheng.html"
					  	})
					  } else {
					  	mui.openWindow({
					  		url: "../kecheng/kecheng2.html",
					  		id: "kecheng2.html"
					  	})
					  }
			// speak("已帮您打开课程查询");
		} else if (s.search("成绩") != -1) {
					   if (localStorage.getItem("userRole") == "学生") {
					  	mui.openWindow({
					  		url: "../chengji/chengji.html",
					  		id: "chengji.html"
					  	})
					  } else {
					  	mui.toast("您不是学生用户哦");
					  }
			// speak("已帮您打开成绩查询");
		} else if (s.search("报修") != -1) {
			mui.openWindow({
				url: "../sever/repairlist.html",
				id: "repairlist.html"
			})
			// speak("已帮您打开学校报修");
		} else if (s.search("图书") != -1) {
			mui.openWindow({
				url: "../tushuguan/index.html",
				id: "tushuguan.html"
			})
			// speak("已帮您打开图书馆");
		} else if (s.search("一卡") != -1) {
			mui.openWindow({
				url: "../yikatong/index.html",
				id: "yikatong.html"
			})
			// speak("已帮您打开一卡通查询");
		} else if (s.search("失物") != -1) {
			mui.openWindow({
				url: "../lostAndFind/index.html",
				id: "lostAndFind.html"
			})
			// speak("已帮您打开失物招领");
		} else if (s.search("修改") != -1) {
			mui.openWindow({
				url: "../mimaReset/demo.html",
				id: "mimaReset.html"
			})
			// speak("已帮您打开修改密码");
		}else if (s.search("工资") != -1) {
			mui.openWindow({
				url: "../caiwu/caiwu.html",
				id: "caiwu.html"
			})
			// speak("已帮您打开工资查询");
		}else if (s.search("收藏") != -1) {
			mui.openWindow({
				url: "../more/index.html",
				id: "more.html"
			})
			// speak("已帮您打开工资查询");
		}else if(s.search("概况") != -1) {
					  createWebview('http://www.smxpt.cn/xqzl/xxjj.htm','gkpage.html','学校概况');
		}else if(s.search("照片") != -1||s.search("墙") != -1) {
					  mui.openWindow({
					  	url: '../xiaoYuanQuan/index.html',
					  	id: 'xiaoYuanQuan.html'
					  })
		}else if(s.search("就业") != -1||s.search("招聘") != -1) {
					  mui.openWindow({
					  	url: '../jiuye/index.html',
					  	id: 'jiuye.html'
					  })
		}else if(s.search("活动") != -1) {
					  mui.openWindow({
					  	url: '../schoolActivity/index.html',
					  	id: 'schoolActivity.html'
					  })
		}else if(s.search("通讯录") != -1||s.search("通讯") != -1) {
					  if (localStorage.getItem("userRole") == "学生") {
					  	mui.toast("您不是教师用户哦");
					  } else {
						  mui.openWindow({
						  	url: '../txl/index.html',
						  	id: 'txl.html'
						  })
					  }
		}else if(s.search("要闻") != -1) {
					  createWebview('https://www.smxpt.cn/xwzx/zhxw.htm','zhxw.htm','学院要闻');
		}else if(s.search("发现") != -1) {
					  mui.openWindow({
					  	url: "../foundInfo/index.html",
					  	id: "foundInfo.html"
					  })
		}else if(s.search("应用") != -1) {
					  mui.openWindow({
					  	url: "../appManager/index.html",
					  	id: "appManager.html"
					  })
		}else if(s.search("校外报名") != -1||s.search("校外") != -1||s.search("校外考试") != -1) {
					  mui.openWindow({
					  	url: '../xwksBaoming/index.html',
					  	id: 'xwksBaoming.html'
					  })
		}else if(s.search("勤工") != -1||s.search("助学") != -1||s.search("勤工助学") != -1) {
					  mui.openWindow({
					  	url: '../qinggongzhuxue/index.html',
					  	id: 'qinggongzhuxue.html'
					  })
		}else if(s.search("顶岗") != -1||s.search("实习") != -1) {
					  launchStudy();
		}else if(s.search("在线") != -1||s.search("在线学习") != -1) {
					 launchStudy();
		}else if(s.search("问卷投票") != -1||s.search("投票") != -1||s.search("问卷") != -1) {
					  mui.openWindow({
					  	url: '../wenjuanToupiao/index.html',
					  	id: 'wenjuanToupiao.html'
					  })
		}else if(s.search("一张表") != -1) {
					  createWebview('../oneSheet/index.html','oneSheet.html','一张表')
		}else if(s.search("食堂评价") != -1||s.search("评价") != -1||s.search("食堂") != -1) {
					  mui.openWindow({
					  	url: '../shitangPingjia/shitangPingjia.html',
					  	id: 'shitangPingjia.html'
					  })
		}else if(s.search("校情统计") != -1||s.search("校情") != -1||s.search("统计") != -1) {
					  createWebview('../schoolTongji/demo.html','schoolTongji.html','校情统计');
		}else if(s.search("校园网") != -1) {
					 mui.openWindow({
					 	url: "../schoolWeb/index.html",
					 	id: "schoolWeb.html"
					 })
		}else if(s.search("学生安全") != -1||s.search("安全") != -1) {
					  if (localStorage.getItem("userRole") == "学生") {
					  	mui.toast("您不是教师用户哦");
					  } else {
					  	createWebview('../studentSecurity/index.html','studentSecurity.html','学生安全');
					  }
		}else if(s.search("移动OA") != -1||s.search("OA") != -1) {
					  if (localStorage.getItem("userRole") == "学生") {
					  	mui.toast("您不是教师用户哦");
					  } else {
					  	createWebview('http://h5.smxpt.cn/singleLogin.jsp?openid=oZW7_0nSP_jYoa9bYxVddS9WYm1g&loginName=null','oApage.html','移动OA')
					  }
		}
	}, function(e) {
		mui.toast("对不起，我没有听明白");
	});
}

//启动学习通 
function launchStudy() {
        // window.location.href = "cxstudy://cxstudy/login?fid=xxx&fidType=xxx&token=xxx"; //唤醒并自动登录
        window.location.href = "cxstudy://"; //仅唤醒
        setTimeout(function() {
            if (!document.webkitHidden) {
				plus.runtime.openURL("https://apps.chaoxing.com/");
            }
        }, 1000);
    }
//语音播报
myCommonJs.speak = function(contents) {
	var speaktext = contents; //传入的值是需要播报的内容
	switch (plus.os.name) {
		case "iOS":
			var AVSpeechSynthesizer = plus.ios.importClass("AVSpeechSynthesizer");
			var AVSpeechUtterance = plus.ios.importClass("AVSpeechUtterance");
			var AVSpeechSynthesisVoice = plus.ios.import("AVSpeechSynthesisVoice");
			var sppech = new AVSpeechSynthesizer();
			var voice = AVSpeechSynthesisVoice.voiceWithLanguage("zh-CN");
			var utterance = AVSpeechUtterance.speechUtteranceWithString(speaktext);
			utterance.setVoice(voice);
			sppech.speakUtterance(utterance);
			plus.ios.deleteObject(voice);
			plus.ios.deleteObject(utterance);
			plus.ios.deleteObject(sppech);
			break;
		case "Android":
			var main = plus.android.runtimeMainActivity();
			var SpeechUtility = plus.android.importClass("com.iflytek.cloud.SpeechUtility");
			SpeechUtility.createUtility(main, "appid=5a20bab9");
			var SynthesizerPlayer = plus.android.importClass("com.iflytek.cloud.SpeechSynthesizer");
			var play = SynthesizerPlayer.createSynthesizer(main, null);
			play.startSpeaking(speaktext, null);
			break;
	}


}

//系统输出		
myCommonJs.log = function(text) {
	console.log(text);
}
//系统输出JSOn
myCommonJs.jsonLog = function(text) {
	console.log(JSON.stringify(text));
}

//原生打开页面

//监听app退出到后台
document.addEventListener("background", function() {
	console.log("12311")
}, false);

//日活
// 当

onAppReume();
document.addEventListener("plusready", onPlusReady, false);

function onPlusReady() {
	document.addEventListener("resume", onAppReume, false);
}

function onAppReume() {
	// alert("Application resumed!");
	myCommonJs.productAjax(apiUrl.productURL1 + apiUrl.activeCount, {
		userCode: localStorage.getItem('userCode')
	}, function(res) {
		// console.log(JSON.stringify(res))
	}, function(a, type, c) {
		console.log(type);
	})
}

//判断是否为手机号
myCommonJs.isPhoneNum = function(str) {
	var myreg = /^[1][3,4,5,7,8][0-9]{9}$/;
	if (!myreg.test(str)) {
		return false;
	}else{
		return true;
	}
}
