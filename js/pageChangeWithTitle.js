var nw = null;
function createWebview(url,id,titleText) {
	nw = plus.webview.create(url,id,{
			popGesture: 'hide',
			// backButtonAutoControl:'close',
			titleNView: {
				backgroundColor: '#208BDD',
				progress: {
					color: '#0000FF'
				},
				titleText:titleText,
				titleColor:"#fff",
				errorPage:'../src/building/demo.html',
				statusbar:{background:'#208bdd'},
				// tags:[{tag:'font',id:'font',text:'原生子View控件',textStyles:{size:'18px'}}],
				// autoBackButton:true
			buttons:[{text:'返回',fontSize:"14px",float:'left',onclick:function(e){
														console.log(JSON.stringify(e));
														nw.canBack(function(errmsg){
															console.log(JSON.stringify(errmsg));
															if(errmsg.canBack){
																nw.back();
															}else{
																nw.close();
															}
														})
													}}]
				
			}
		}),
		//正在加载
		nw.addEventListener('loading', function(e) {
			plus.nativeUI.showWaiting();
			console.log('Loading: ' + nw.getURL());
		}, false);
		//加载成功
		nw.addEventListener('loaded', function(e) {
			plus.nativeUI.closeWaiting();
			console.log('Loaded: ' + nw.getURL());
		}, false);
		//窗口show事件
		nw.addEventListener('show', function(e) {
			console.log('Webview Showed');
			plus.nativeUI.showWaiting();
		}, false);
		//窗口隐藏事件
		nw.addEventListener('hide', function(e) {
			console.log('Webview Hided');
			plus.nativeUI.closeWaiting();
		}, false);
		//关闭
		// nw.addEventListener('close', function() {
		// 	plus.nativeUI.closeWaiting();
		// 	console.log('close: ' + nw.getURL());
		// 	nw = null;
		// }, false);
		//窗口加载错误
		nw.addEventListener('error', function(e) {
			plus.nativeUI.closeWaiting();
			console.log('Error: ' + nw.getURL());
			plus.nativeUI.alert("页面错误",function(){
				mui.back();
			})
		}, false);
		//窗口渲染中
		nw.addEventListener('rendering', function(e) {
			console.log('Webview rendering');
		}, false);
		//渲染完毕
		nw.addEventListener('rendered', function(e) {
			// console.log('Webview rendered');
			plus.nativeUI.closeWaiting();
		}, false);
		//标题更新
		nw.addEventListener('titleUpdate', function(e) {
			plus.nativeUI.closeWaiting();
			if(e.title=="Error"){
				plus.nativeUI.alert("第三方页面无法访问",function(){
					mui.back();
				})
				}
			
			console.log('Update title: ' + e.title);
		}, false);
		//窗口触屏事件
		nw.addEventListener('touchstart', function(e) {
			console.log('touchstart：'+e);
		}, false);
		//窗口触屏事件
		nw.addEventListener('popGesture', function(e) {
			//		poplog.innerText='popGesture: '+e.type+','+e.result+','+e.progress;
			console.log('popGesture: ' + e.type + ',' + e.result + ',' + e.progress)
			
		}, false);
		//窗口进度条事件
		nw.addEventListener('progressChanged', function(e) {
			// e.progress可获取加载进度
			//		outlog.innerText='Progress Changed: '+e.progress+'%';
			// console.log('Progress Changed: ' + JSON.stringify(e));
			plus.nativeUI.closeWaiting();
		}, false);
		nw.show('pop-in');
		return nw;
}

function fnBack(){
	mui.back();
}
