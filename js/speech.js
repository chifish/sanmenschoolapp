//语音识别
var text=null;
function startRecognize() {
	if(plus.os.name=='Android'&&navigator.userAgent.indexOf('StreamApp')>0){
		plus.nativeUI.toast('当前环境暂不支持语音识别插件');
		return;
	}
	var options = {};
	options.engine = 'iFly';
	//options.punctuation = false;	// 是否需要标点符号 
	text.value = "";
	outSet( "开始语音识别：" );
	plus.speech.startRecognize( options, function ( s ) {
		outLine( s );
		text.value += s;
	}, function ( e ) {
		outSet( "语音识别失败："+e.message );
	} );
}
function startRecognizeEnglish(){
	if(plus.os.name=='Android'&&navigator.userAgent.indexOf('StreamApp')>0){
		plus.nativeUI.toast('当前环境暂不支持语音识别插件');
		return;
	}
	var options = {};
	options.engine = 'iFly';
	options.lang = 'en-us';
	text.value = "";
	outSet( "开始语音识别：" );
	plus.speech.startRecognize( options, function ( s ) {
		outLine( s );
		text.value += s;
	}, function ( e ) {
		outSet( "语音识别失败："+e.message );
	} );
}