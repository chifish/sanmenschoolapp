apiUrl = {
	productURL:'http://125.46.98.227:12333/smxzyapp/core/',
	productURL1:'http://125.46.98.227:12333/smxzyapp/',
	imageUrlPro:"http://125.46.98.227:12333/smxzyapp/opt/data/avatarDir/",
	rootProductUrl:"http://125.46.98.227:12333/smxzyapp/opt/data",
// 	productURL:'http://125.46.98.227:12333/smxzyapp/core/',
// 	productURL1:'http://125.46.98.227:12333/smxzyapp/',
// imageUrlPro:"http://125.46.98.227:12333/smxzyapp/opt/data/avatarDir/",
	debugURL:'http://192.168.1.5:8088/sanzhiyuan/core/',
	debugURL1:'http://192.168.1.5:8088/sanzhiyuan/',
	imageUrl:"http://192.168.1.10:11111/avatarDir/",
	rootUrl:"http://192.168.1.10:11111",
	// rootProductUrl:"http://125.46.98.227:12333/smxzyapp/opt/data",
	// rootProductUrlSmall:"http://125.46.98.227:12333/smxzyapp/opt/data/small",
	meetingSearch:'app-meeting!meetingSearch.action',	//会议查询 
	appClassStudentSearch:'app-class-student!appClassStudentSearch.action ',// 学生课程查询
	appClassTeacherSearch:'app-class-teacher!appClassTeacherSearch.action',// 教师课程查询
	changeHeadImage:'app-head-image!changeHeadImage.action',//头像 
	appSinMeeting:'app-meeting!appSinMeeting.action',	//会议签到
	appLeaveMeeting:'app-leave-apply!appSinMeeting.action',//会议请假
	appGradeSearch:'app-grade!appGradeSearch.action',	//成绩查询
	appPictureSearch:'app-picture-collection!appPictureSearch.action',	//照片墙  
	appPictureApply:'app-picture-collection!appPictureApply.action',	//上传图片
		//校历概况 
	appRepairSearch:'app-repair!appRepairSearch.action',//报修查询
	appRepairApply:'app-repair!appRepairApply.action',//报修申请
	appJudgeApply:'app-repair!appJudgeApply.action',//报修打分
	appDisplay:'app-repair!appDisplay.action',//取消报修
	appDisRepairSearch:'app-repair!appDisRepairSearch.action',//报修评分查询
	appBookSearchjy:'app-book-store-jy!appBookSearch.action',	//图书馆借阅  
	appBookSearchwj:'app-book-store-wj!appBookSearch.action',	//查询
	appOneCardXfSearch:'app-one-card!appOneCardXfSearch.action',	//一卡通消费查询
	appOneCardCzSearch: 'app-one-card!appOneCardCzSearch.action',	//一卡通充值查询
	appLostAndFoundSearch:'app-lost-and-found!appLostAndFoundSearch.action',//失物招领查询
	appLostAndFound:'app-lost-and-found!appLostAndFound.action',//失物招领申请
	appComments:'app-lost-and-found!appComments.action',//失物招领评论
	appReplays:'app-lost-and-found!appReplays.action',//失物招领评论回复
	appDisplaylf:'app-lost-and-found!appDisplay.action',//撤销帖子
	appSalarySearch:'app-salary!appSalarySearch.action',	//工资查询  
	login:'app-login.action',	//登录
	logout:'app-login!logout.action',	//退出
	scanLogin:'login!scanLogin.action',//扫码登陆
	activeCount:'app-login!activeCount.action',//日活
	appDisplays:'app-change-password!appDisplay.action',//修改密码
	appRecruitSearch:'app-recruit!appRecruitSearch.action',// 就业招聘
	appRecruitDetilSearch:'app-recruit!appRecruitDetilSearch.action',
	appActivititySearch:'app-campus-activitity!appActivititySearch.action',//校园活动搜索
	appPublis:'app-campus-activitity!appPublis.action',//发布活动
	appSinActive:'app-campus-activitity!appSinActive.action',//活动签到
		// 新闻公告主页
		// 头像主界面图片
	appOutSchoolExamSearch:'app-out-school-exam!appOutSchoolExamSearch.action',	//校外考试报名查询
	appOutSchoolExamApply:'app-out-school-exam!appOutSchoolExamApply.action',	//校外考试报名提交
	appOutSchoolDetilSearch:'app-out-school-exam!appOutSchoolDetilSearch.action',//校外考试报名详情
	appPartTimeJobListSearch:'app-part-time-job!appPartTimeJobListSearch.action',	//勤工助学报名查询
	appPartTimeJobApply:'app-part-time-job!appPartTimeJobApply.action',	//勤工助学报名提交
	appJobDetilSearch:'app-part-time-job!appJobDetilSearch.action',//勤工助学报名详情
	// app-work-study!appWorkStudySearch.action
	appWorkStudySearch:'app-work-study!appWorkStudySearch.action',//问卷和投票查询
	// app-work-study!appSubmitDcwj.action
	appSubmitDcwj:'app-work-study!appSubmitDcwj.action',//问卷，投票 提交
	
	// app-work-study!appWorkStudyDetail.action
	appWorkStudyDetail:'app-work-study!appWorkStudyDetail.action',//问卷，投票详情
	apptpCount:'app-work-study!apptpCount.action',//投票结果
	appVersion:'app-version!appVersion.action',//版本管理
	// app-school-news!appSchoolNewsListSearch.action
	appSchoolNewsListSearch:'app-school-news!appSchoolNewsListSearch.action',//新闻列表
	appSchoolNewsListSearchRss:'app-school-news!appSchoolNewsSearchRss.action',//新闻标题RSS
	appSchoolNewsDetilSearch:'app-school-news!appSchoolNewsDetilSearch.action',//新闻列表详情
	appInfoSearch:'app-user-info!appInfoSearch.action',//个人信息查询
	appPhoneApply:'app-user-info!appPhoneApply.action',//个人信息提交手机号
	appFoundInfoSearch:'app-found-info!appFoundInfoSearch.action',//发现查询
	appFoundInfoDetail:'app-found-info!appFoundInfoDetail.action',//发现详情
	appManagerListSearch:'app-manager-list!appManagerListSearch.action', //应用管理列表
	appCampusActivitityApply:'app-campus-activitity!appCampusActivitityApply.action',//校园活动申请
	appBookSearchOne:'app-mail-book!appBookSearchOne.action' ,//通讯录详情
	appBookSearch:'app-mail-book!appBookSearch.action',//通讯录查询
	appBookSearchAll:'app-mail-book!appBookSearchAll.action',//通讯录全部查询
	appVoteApply:'app-work-study!appVoteApply.action' //投票申请

}