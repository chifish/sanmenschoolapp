﻿# 项目目录及资源优化
# 对于今后及现有项目进行优化和修改
# 作者：cc
# 2019-03-01

# 1、项目目录 ├──  └── │
projec name
    ├── css                     //全局样式资源
    │
    ├── font                    //字体图标
    │
    ├── js                      //全局js
    │
    ├── src                     //页面资源
         │
         └──webViewName         //总功能模块名称
                ├──css          //此模块用到的js
                ├──images       //此模块所用的所有本地图片资源
                ├── js          //此模块用到的js
                ├──webViewName  //子模块包
                └──index.html   //模块启动页
    │
    ├── static                  //本地静态图片
    │
    ├── unpackage               //App图标、启动页
    │
    │
    └── manifest.json           //App配置文件
# 2、项目优化 
   # 1、全局JS中增加
   #    api.js,      //所有的接口地址 json形式
   #    myCommonJS.js,//所有的公共方法，简化页面复杂度，封装较长的方法
   #    root.js        //页面本地相对地址
   # 2、html页面代码减少
   #    模块中增加index.js 模板文件  //包含下拉刷新 、 上拉加载 、接口请求 、页面渲染 、 保护页面 